<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	public function initialize(){
		// Add some local CSS resources
		$this->assets
			->addCss('css/bootstrap.min.css')
			->addCss('css/hallooou.css')
			->addCss('css/colors.css')
			->addCss('css/plugins/owl.carousel.css')
			->addCss('css/plugins/owl.theme.css')
			->addCss('css/plugins/owl.transitions.css')
			->addCss('css/plugins/animate.css')
			->addCss('css/plugins/magnific-popup.css')
			->addCss('css/plugins/jquery.mb.YTPlayer.min.css')
			->addCss('css/font-awesome.min.css');
			// And some local JavaScript resources
		$this->assets
			->collection('footer')
			->addJs('js/jquery.min.js')
			->addJs('js/bootstrap.min.js')
			->addJs('js/jquery.touchSwipe.js')
			->addJs('js/jquery.horizonScroll.js')
			->addJs('js/slick.min.js')
			->addJs('js/plugins/wow.min.js')
			->addJs('js/plugins/owl.carousel.min.js')
			->addJs('js/plugins/jquery.parallax-1.1.3.js')
			->addJs('js/plugins/jquery.magnific-popup.min.js')
			->addJs('js/plugins/jquery.mb.YTPlayer.min.js')
			->addJs('js/plugins/jquery.countTo.js')
			->addJs('js/plugins/jquery.inview.min.js')
			->addJs('js/plugins/pace.min.js')
			->addJs('js/plugins/jquery.easing.min.js')
			->addJs('js/plugins/jquery.nicescroll.min.js')
			->addJs('js/plugins/jquery.validate.min.js')
			->addJs('js/plugins/additional-methods.min.js')
			->addJs('js/isotope.pkgd.min.js')
			->addJs('js/hallooou.js');
	}
}
