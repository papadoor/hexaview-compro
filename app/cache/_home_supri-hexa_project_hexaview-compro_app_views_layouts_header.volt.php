<nav class="navbar navbar-custom navbar-fixed-top top-nav-collapse" role="navigation">
    <div class="container">
        <div class="navbar-header pull-left">
            <a class="navbar-brand page-scroll" href="#page-top">
                <!-- replace with your brand logo/text -->
                <span class="brand-logo"><img src="images/hexa-logo.png" onerror="this.src='images/logo.png'; this.onerror=null;" alt="Hallooou - HTML5 Template" title="Hallooou - HTML5 Template" class="img-responsive"></span>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="ta active">
                <a href="#section-section1"><span>WORK</span></a>
            </li>
            <li class="ta">
                <a href="#section-section2"><span>ABOUT</span></a>
            </li>
            <li class="ta">
                <a href="#section-section3"><span>CAREERS</span></a>
            </li>
            <li class="ta">
                <a href="#section-section4"><span>CONTACT</span></a>
            </li>
        </ul>
    </div><!-- /.container -->
</nav>
