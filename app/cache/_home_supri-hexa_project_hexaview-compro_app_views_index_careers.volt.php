
  <section id="section-section3" class="careers content-section alt-bg-light">
                <div class="title-contentNext titles"><h1>CONTACT.</h1></div>
        <div class="container">
            <div class="row">
                <div class="title-content"><h1>CAREERS.</h1></div>
                <div class="body-content">
                    <div class="row careers-slider">
                        <div class="item col-xs-4 col-md-3 i">
                            <div class="card-careers">
                            <div class="icon">
                              <img src="images/icons/graphic-design.png" alt="icons">
                              </div>
                              <a href="#">
                                  <div class="title-careers">
                                      <h2>Grapich Designer</h2>
                                  </div>
                              </a>
                              <p>The Graphic Designer provides Accurate artwork in accordance to brief to highest standarts.</p>
                              <p>career@hexaview.com</p>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-3 i">
                            <div class="card-careers">
                            <div class="icon">
                              <img src="images/icons/writer.png" alt="icons">
                              </div>
                              <a href="#">
                                  <div class="title-careers">
                                      <h2>Copywriter</h2>
                                  </div>
                              </a>
                              <p>The Graphic Designer provides Accurate artwork in accordance to brief to highest standarts.</p>
                              <p>career@hexaview.com</p>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-3 i">
                            <div class="card-careers">
                            <div class="icon">
                              <img src="images/icons/photo-camera.png" alt="icons">
                              </div>
                              <a href="#">
                                  <div class="title-careers">
                                      <h2>Photographer</h2>
                                  </div>
                              </a>
                              <p>The Graphic Designer provides Accurate artwork in accordance to brief to highest standarts.</p>
                              <p>career@hexaview.com</p>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-3 i">
                            <div class="card-careers">
                            <div class="icon">
                              <img src="images/icons/growth.png" alt="icons">
                              </div>
                              <a href="#">
                                  <div class="title-careers">
                                      <h2>Accounting</h2>
                                  </div>
                              </a>
                              <p>The Graphic Designer provides Accurate artwork in accordance to brief to highest standarts.</p>
                              <p>career@hexaview.com</p>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-3 i">
                            <div class="card-careers">
                            <div class="icon">
                              <img src="images/icons/travel.png" alt="icons">
                              </div>
                              <a href="#">
                                  <div class="title-careers">
                                      <h2>GA</h2>
                                  </div>
                              </a>
                              <p>The Graphic Designer provides Accurate artwork in accordance to brief to highest standarts.</p>
                              <p>career@hexaview.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
