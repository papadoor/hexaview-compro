<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Hexaview</title>
        <?= $this->assets->outputCss() ?>
        <style type="text/css">
section {
    float: left;
    display: block;
    height: 100%;
    padding: 75px 0 0 0;
    margin: 0;
}

#section-section1 {
    background: #F5F5F5;
}

#section-section2 {
    background: #F5F5F5;
}

#section-section3 {
    background: #F5F5F5;
}

#section-section4 {
    background: #F5F5F5;
}

.horizon-prev, .horizon-next {
    position: fixed;
    top: 50%;
    margin-top: -24px;
    z-index: 9999;
}

.horizon-prev {
    left: 20px;
}

.horizon-next {
    right: 20px;
}
</style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <script>
        document.createElement('video');
      </script>
    <![endif]-->
    </head>
    <body id="home">
            <?= $this->partial('layouts/header') ?>
            <?= $this->getContent() ?>
            <?= $this->partial('layouts/footer') ?>
    </body>
</html>
