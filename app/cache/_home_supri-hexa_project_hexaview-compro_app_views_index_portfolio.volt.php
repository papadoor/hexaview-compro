
  <section id="section-section3" class="team content-section">
    <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Happy &amp; excited about Hallooou</h2>
                    <h3 class="caption gray">Customers in 100+ countries use our services</h3>
                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of existence.</p>

                    <blockquote>
                        There are two types of people who will tell you that you cannot make a difference in this world: those who are afraid to try and those who are afraid you will succeed.
                        <span>Ray Goforth</span>
                    </blockquote>

                </div><!-- /.col-md-6 -->

                <div class="col-md-6">
                    <!-- Display image -->
                    <img src="images/portfolio/1.jpeg" class="img-responsive">
                    <!-- Optional video embed code -->
                   <!--  <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://player.vimeo.com/video/132613118?title=0&amp;byline=0&amp;portrait=1" class="embed-responsive-item" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div> -->
                </div>
            </div>
            <footer>
              <div class="copynote">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-12 text-center">
                              &copy; 2017. Hexaview. All rights reserved.
                          </div><!-- /.col-md-12 -->

                      </div><!-- /.row -->
                  </div><!-- /.container -->
              </div><!-- /.copynote -->

              <div class="nav pull-right scroll-top">
                  <a href="#home" title="Scroll to top"><i class="fa fa-angle-up"></i></a>
              </div>

          </footer>
        </div>
</section>
