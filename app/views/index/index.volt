
{% block work %}
  {{ partial("index/work") }}
{% endblock %}
{% block about %}
  {{ partial("index/about") }}
{% endblock %}
{% block careers %}
  {{ partial("index/careers") }}
{% endblock %}
{% block contact %}
  {{ partial("index/contact") }}
{% endblock %}