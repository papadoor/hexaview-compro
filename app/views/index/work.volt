{% block work %}
    <section id="section-section1" class="work content-section alt-bg-light">
                <div class="title-contentNext titles"><h1>ABOUT US.</h1></div>
        <div class="container">
            <div class="row">
                <div class="title-content"><h1>OUR WORK.</h1></div>
                <div class="body-content">
                    <div class="row work-slider">
                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>

                        <div class="item col-xs-4 col-md-2 i">
                            <div class="card">
                              <img src="images/img_avatar.png" alt="Avatar" style="width:100%">
                              <a href="#">
                                  <div class="container-avatar">
                                    <div class="category-card">CAMPAIGN</div>
                                    <div class="title-card">John Doe</div>
                                  </div>
                              </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{% endblock %}