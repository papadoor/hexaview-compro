{% block about %}
    <section id="section-section2" class="about content-section alt-bg-light">
        <div class="title-contentNext titles"><h1>CAREERS.</h1></div>
        <div class="container">
            <div class="row" style="min-height: 870px">
                <div class="title-content"><h1>ABOUT US.</h1></div>
                <div class="body-content">
                    <div class="col-md-7">
                        <div class="title-body-content"><h2>What We Do</h2></div>
                        <p class="text-body-content">We design for the future-in ways that make sense right now. We're strategic experts and creative explorers committed to solving complex challenges. With smart tools and globl resources, we examine the implications of every brand choice, create new experiences, and open doors to opportunity.</p>

                    </div><!-- /.col-md-6 -->

                    <div class="col-md-5">
                        <div class="embed-responsive embed-responsive-16by9 wrap-video">
                            <div class="thumb-video">
                                <a id="start" class="start-video">
                                    <img width="64" src="http://image.flaticon.com/icons/png/512/0/375.png" >
                                </a>
                            </div>
                            <iframe id="video" class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/eBR4Yu6Gnfo" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <!-- <div id="player"></div>
                        <div class="thumb-video">
                            <a class="start-video"><img width="64" src="http://image.flaticon.com/icons/png/512/0/375.png" style="filter: invert(100%); -webkit-filter: invert(100%);"></a>
                        </div> -->
                    </div>
                </div>
                <div class="show-carousel">
                    <a href="#carousel-about" id="scrols" title="Show"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div>
        <div id="carousel-about" style="min-height: 900px">
            <div class="container-about">
                <div class="container">

                    <div class="row">
                        <div class="about-slider">
                            <div class="item">
                                <img src="images/single-slider/slide1.jpg" alt="The Last of us">
                                <div class="about-info">
                                    <h3>Branding</h3>
                                    <p>We have Expertise in all marketing disciplines such as brand & strategy development, brand positioning, campign planning, and creative development & execution.</p>
                                    <p>We belive that great work is something conceived, nurtured, produced, and evaluated together.</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="images/single-slider/slide1.jpg" alt="GTA V">
                                <div class="about-info">
                                    <h3>Event</h3>
                                    <p>We have Expertise in all marketing disciplines such as brand & strategy development, brand positioning, campign planning, and creative development & execution.</p>
                                    <p>We belive that great work is something conceived, nurtured, produced, and evaluated together.</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="images/single-slider/slide1.jpg" alt="Mirror Edge">
                                <div class="about-info">
                                    <h3>Digital</h3>
                                    <p>We have Expertise in all marketing disciplines such as brand & strategy development, brand positioning, campign planning, and creative development & execution.</p>
                                    <p>We belive that great work is something conceived, nurtured, produced, and evaluated together.</p>
                                </div>
                            </div>
                             <div class="item">
                                <img src="images/single-slider/slide1.jpg" alt="Mirror Edge">
                                <div class="about-info">
                                    <h3>Advertising</h3>
                                    <p>We have Expertise in all marketing disciplines such as brand & strategy development, brand positioning, campign planning, and creative development & execution.</p>
                                    <p>We belive that great work is something conceived, nurtured, produced, and evaluated together.</p>
                                </div>
                            </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="container client">
                <div class="row client-slider">
                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/sony-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/nike-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/paramount-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/microsoft-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/citibank-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/fedex-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/sony-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/nike-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/paramount-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/microsoft-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/citibank-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                    <div class="item col-xs-4 col-md-2 i">
                        <a href="#" title="#">
                            <img src="images/fedex-logo.png" class="img-responsive">
                        </a>
                    </div><!-- /.col-xs-4 -->

                </div>
            </div>
            <div class="scroll-top">
                <a href="#section-section2" id="scroll-top" title="Hide"><i class="fa fa-angle-up"></i></a>
            </div>
        </div>
    </section>
{% endblock %}
