{% block contact %}
    <section id="section-section4" class="contact content-section alt-bg-light">
        <div class="container">
            <div class="row" style="min-height: 870px">
                <div class="title-content"><h1>CONTACT US.</h1></div>
                <div class="title-contentNext"></div>
                <div class="body-content">
                    <div class="col-md-6">
                        <div class="title-body-content"><h2>KEEP IN TOUCH</h2></div>
                        <p class="text-body-content">Jalan Pantai Kuta no.2, RT/RW.016, Ancol, Jakarta, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14430</p>
                        <!-- <div class="gate-down"></div> -->


                    </div><!-- /.col-md-6 -->

                    <div class="col-md-6">
                        <!-- <div class="container-fluid buffer-forty-top"> -->
                            <div id="cd-google-map no-bottom-pad">
                                <div id="google-container"></div>
                                <div id="cd-zoom-in"></div>
                                <div id="cd-zoom-out"></div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
                <div class="follow-us">
                    <div>
                        <span class="text-follow">Follow us on</span>
                    </div>
                    <i class="fa fa-facebook-square fa-2x"></i>
                    <i class="fa fa-instagram  fa-2x"></i>
                </div>
            </div>
        </div>
    </section>
{% endblock %}
