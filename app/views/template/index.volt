<!DOCTYPE html>
<html>
    <head>
    </head>

    <body>
        <div id="about">{% block about %}{% endblock %}</div>
        <div id="services">{% block services %}{% endblock %}</div>
        <div id="portfolio">{% block portfolio %}{% endblock %}</div>

        <div id="footer">
            {% block footer %}&copy; Copyright 2015, All rights reserved.{% endblock %}
        </div>
    </body>
</html>